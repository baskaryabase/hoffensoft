// "use strict";

export default function imgReducer(
  state = {
    images: []
  },
  action
) {
  switch (action.type) {
    case "getImages":
      return {
        ...state,
        images: action.payload
      };
    // break;
  }

  return state;
}
