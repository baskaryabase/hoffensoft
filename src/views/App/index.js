import React from "react";
import { FormGroup, Label, Input, Container, Row } from "reactstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getImages } from "../../actions/imgAction";
import ImgView from "../../components/ImgView";

function App(props) {
  return (
    <div className="App">
      <Container>
        <FormGroup>
          <Label for="exampleEmail">Search</Label>
          <Input
            type="text"
            name="search"
            onKeyUp={e => props.getImages(e.target.value)}
            placeholder="Search an image"
          />
        </FormGroup>
      </Container>
      <Container>
        <Row>
          {props.images.map((img, i) => (
            <ImgView
              index={i}
              src={img.urls.thumb}
              regular={img.urls.regular}
              images={props.images}
            />
          ))}
        </Row>
      </Container>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    images: state.images
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getImages }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
