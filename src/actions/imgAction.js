export const getImages = query => {
  let url = `https://api.unsplash.com/search/photos/?query=${query}&client_id=df28ff85ddfd5c66e129d901cd2f025463d4911ed156aff171d16e2cf588c07d`;
  return dispatch =>
    new Promise((resolve, reject) => {
      fetch(url).then(async res => {
        let result = await res.json();
        console.log(result);
        dispatch({ type: "getImages", payload: result.results });
      });
    });
};
