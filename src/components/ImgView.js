import React, { useState } from "react";
import {
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";

const ImgView = props => {
  const [modal, setModal] = useState(false);
  const [index, setIndex] = useState(props.index);
  const [image, setImage] = useState(props.regular);
  return (
    <Col xs={12} sm={6} md={4} lg={3}>
      <span
        style={{ cursor: "pointer", height: 100 }}
        onClick={() => setModal(!modal)}
      >
        <img style={{ padding: 5 }} src={props.src} />
      </span>
      <div>
        <Modal
          isOpen={modal}
          toggle={() => setModal(!modal)}
          className={props.className}
        >
          <ModalHeader toggle={() => setModal(!modal)}>Modal title</ModalHeader>
          <ModalBody>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <img style={{ width: 250, alignSelf: "center" }} src={image} />
            </div>
          </ModalBody>
          <ModalFooter>
            <div
              style={{
                display: "flex",
                width:"100%",
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <Button
                outline
                color="primary"
                onClick={() => {
                  setIndex(index - 1);
                  setImage(props.images[index - 1].urls.regular);
                }}
              >
                prev
              </Button>
              <Button
                outline
                color="primary"
                onClick={() => {
                  console.log("next", props.images[index + 1].urls.regular);
                  setIndex(index + 1);
                  setImage(props.images[index + 1].urls.regular);
                }}
              >
                next
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      </div>
    </Col>
  );
};

export default ImgView;
